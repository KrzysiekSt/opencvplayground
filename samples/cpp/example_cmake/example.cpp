#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/imgcodecs.hpp"
#include <iostream>

using namespace::cv;

const int CHANNELS = 3;

struct EdgePixel
{
    int x, y;
    int weight;
};

struct Pixel
{
    int x, y;
};

class ShapeReader
{
public:
    ShapeReader(std::string path);
    void getShape();
    void getColor();
private:

    cv::Mat     image;
    cv::Mat     rotatedMatrix;

    std::vector<EdgePixel> edgePoints;
    std::vector<Pixel> notWhitePixels;

    void collectData();
    void hasTwoDistinctPixelsAround(cv::Point3_<uint8_t> &_, const int position[]);

    bool isSquare();
    bool isCircle();
    bool isTriangle();
    bool isPixelInRange(uint x, uint y);
    bool isPixelNotWhite(uint x, uint y);
    bool isShapePixel(uint x, uint y);
    bool arePixelsSameColor(const Vec3b &a, const Vec3b &b);

    cv::Mat rotateImageByAngle(int angle);

};


ShapeReader::ShapeReader(std::string path) : image(cv::imread(path, cv::IMREAD_COLOR))
{
    collectData();
}

bool ShapeReader::isPixelInRange(uint x, uint y)
{
    if(x != 0 && x != (image.rows - 1) &&
        y != 0 && y != (image.cols - 1))
    {
        return true;
    }
    return false;
}

bool ShapeReader::isPixelNotWhite(uint x, uint y)
{
    Vec3b& imagePixel = image.at<Vec3b>(x, y);
    for(int channel = 0; channel < CHANNELS; channel++)
    {
        if(imagePixel[channel] > 30)
        {
            return true;
        }
    }
    return false;

}

bool ShapeReader::isShapePixel(uint x, uint y)
{
    if(this->isPixelInRange(x, y) && this->isPixelNotWhite(x, y))
    {
        return true;
    }
    return false;
}


bool ShapeReader::arePixelsSameColor(const Vec3b &a, const Vec3b &b)
{
    for(int channel = 0; channel < CHANNELS; channel++)
    {
        if(abs(a[channel] - b[channel]) > 30)
        {
            return false;
        }
    }
    return true;

}

void ShapeReader::collectData()
{
    for(int x = 0; x < image.cols; x++)
    {
        for(int y = 0; y < image.rows; y++)
        {
            int weightCounter = 0;
            if(this->isShapePixel(x, y))
            {
                Vec3b &pixelUnderConsideration = image.at<Vec3b>(x, y);
                std::array<Vec3b, 4> neighbours =
                {
                    image.at<Vec3b>(x, (y - 1)),
                    image.at<Vec3b>(x, (y + 1)),
                    image.at<Vec3b>((x - 1), y),
                    image.at<Vec3b>((x + 1), y)
                };

                for(Vec3b neighbour : neighbours)
                {
                    if(!this->arePixelsSameColor(neighbour, pixelUnderConsideration))
                    {
                        weightCounter++;
                    }
                }

                if(weightCounter > 1)
                {
                    this->edgePoints.emplace_back(EdgePixel{.x = x, .y = y, .weight = weightCounter});
                }
                this->notWhitePixels.emplace_back(Pixel{.x = x, .y = y});
            }
        }
    }

}

void ShapeReader::getShape()
{
    if(isSquare())
    {
        std::cout<<"Square\n";
    }
    if(isTriangle())
    {
        std::cout<<"Triangle\n";
    }
    if(isCircle())
    {
        std::cout<<"Circle\n";
    }
}

void ShapeReader::getColor()
{
    double r = 0, g = 0, b = 0;
    for( auto pixel : notWhitePixels)
    {
        Vec3b &pixelUnderConsideration = image.at<Vec3b>(pixel.x, pixel.y);
        b += pixelUnderConsideration[0];
        g += pixelUnderConsideration[1];
        r += pixelUnderConsideration[2];
    }

    if((r / g) > 1.0 && (r / g) < 1.3)  std::cout<<"Orange\n";
    else
    {
        if(r / (r + g + b) > 0.33)          std::cout<<"Red\n";
        if(g / (r + g + b) > 0.33)          std::cout<<"Green\n";
        if(b / (r + g + b) > 0.33)          std::cout<<"Blue\n";
    }


}


bool ShapeReader::isSquare()
{
    int counter = 0;
    for(auto edgePoint : edgePoints)
    {
        if(edgePoint.weight == 2)
        {
            counter++;
        }
    }
    if(counter == 4)
    {
        return true;
    }
    return false;
}

bool ShapeReader::isTriangle()
{
    int counter = 0;
    for(auto edgePoint : edgePoints)
    {
        if(edgePoint.weight == 3)
        {
            counter++;
        }
    }
    if(counter == 3)
    {
        return true;
    }
    return false;
}

bool ShapeReader::isCircle()
{
    int counter = 0;
    for(auto edgePoint : edgePoints)
    {
        if(edgePoint.weight != 2)
        {
            return false;
        }
    }
    if(edgePoints.size() < 5) return false;
    return true;
}


int main()
{
    //ShapeReader myReader("./greenTriangle.png");
    ShapeReader myReader("./greenSquare.png");
    //ShapeReader myReader("./greenCircle.png");
    //ShapeReader myReader("./redCircle.png");
    //ShapeReader myReader("./orangeSquare.png");
    myReader.getShape();
    myReader.getColor();

    waitKey();
}

